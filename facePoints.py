#https://github.com/lincolnhard/head-pose-estimation/blob/master/video_test_shape.py

import cv2
import dlib
import numpy as np
from imutils import face_utils
import imutils
import base64
import threading
import time
import socket

class Points:

    HOST = '127.0.0.1'  # The server's hostname or IP address
    PORT = 65432        # The port used by the server

    ENCODE_PARAM = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

    face_landmark_path = 'D:/Git/WebAR/shape_predictor_68_face_landmarks.dat'

    K = [6.5308391993466671e+002, 0.0, 3.1950000000000000e+002,
         0.0, 6.5308391993466671e+002, 2.3950000000000000e+002,
         0.0, 0.0, 1.0]
    D = [7.0834633684407095e-002, 6.9140193737175351e-002,
         0.0, 0.0, -1.3073460323689292e+000]

    cam_matrix = np.array(K).reshape(3, 3).astype(np.float32)
    dist_coeffs = np.array(D).reshape(5, 1).astype(np.float32)

    object_pts = np.float32([[6.825897, 6.760612, 4.402142],
                             [1.330353, 7.122144, 6.903745],
                             [-1.330353, 7.122144, 6.903745],
                             [-6.825897, 6.760612, 4.402142],
                             [5.311432, 5.485328, 3.987654],
                             [1.789930, 5.393625, 4.413414],
                             [-1.789930, 5.393625, 4.413414],
                             [-5.311432, 5.485328, 3.987654],
                             [2.005628, 1.409845, 6.165652],
                             [-2.005628, 1.409845, 6.165652],
                             [2.774015, -2.080775, 5.048531],
                             [-2.774015, -2.080775, 5.048531],
                             [0.000000, -3.116408, 6.097667],
                             [0.000000, -7.415691, 4.070434]])

    reprojectsrc = np.float32([[10.0, 10.0, 10.0],
                               [10.0, 10.0, -10.0],
                               [10.0, -10.0, -10.0],
                               [10.0, -10.0, 10.0],
                               [-10.0, 10.0, 10.0],
                               [-10.0, 10.0, -10.0],
                               [-10.0, -10.0, -10.0],
                               [-10.0, -10.0, 10.0]])

    line_pairs = [[0, 1], [1, 2], [2, 3], [3, 0],
                  [4, 5], [5, 6], [6, 7], [7, 4],
                  [0, 4], [1, 5], [2, 6], [3, 7]]

    def __init__(self):
        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(self.face_landmark_path)
        print("dlib started...")

        self.dimensions_video = (640, 480)
        self.dimensions_face = (640, 480)
        print("###dimensions###|{}".format(self.dimensions_face))

    def get_head_pose(self, shape):
        image_pts = np.float32([shape[17], shape[21], shape[22], shape[26], shape[36],
                                shape[39], shape[42], shape[45], shape[31], shape[35],
                                shape[48], shape[54], shape[57], shape[8]])

        _, rotation_vec, translation_vec = cv2.solvePnP(
            self.object_pts, image_pts, self.cam_matrix, self.dist_coeffs)

        reprojectdst, _ = cv2.projectPoints(self.reprojectsrc, rotation_vec, translation_vec, self.cam_matrix,
                                            self.dist_coeffs)

        reprojectdst = tuple(map(tuple, reprojectdst.reshape(8, 2)))

        # calc euler angle
        rotation_mat, _ = cv2.Rodrigues(rotation_vec)
        pose_mat = cv2.hconcat((rotation_mat, translation_vec))
        _, _, _, _, _, _, euler_angle = cv2.decomposeProjectionMatrix(pose_mat)

        return reprojectdst, euler_angle

    def SendSocketMesg(self, msg):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.HOST, self.PORT))
            msg = msg + "@@!!@@"
            s.sendall(str.encode(msg))
            data = s.recv(1024*128)
            return data
            #print("Response: {}".format(data))

    def GetFacePoints(self, frame):
        # Scale frame to analize faces
        #frame = cv2.resize(frame, self.dimensions_video)

        # Send frame to Unity
        result, buffer = cv2.imencode('.jpg', frame, self.ENCODE_PARAM)
        jpg_as_text = base64.b64encode(buffer)
        
        fullMessage = "@@&&@@|{}".format(jpg_as_text.decode("utf-8"))

        # Scale frame to analize faces
        frame = cv2.resize(frame, self.dimensions_face)

        # get faces
        face_rects = self.detector(frame, 0)

        facePoints = {}

        if len(face_rects) > 0:
            for i, d in enumerate(face_rects):
                shape = self.predictor(frame, d)
                shape = face_utils.shape_to_np(shape)

                reprojectdst, euler_angle = self.get_head_pose(shape)

                facePoints = {
                    "faceId": i,
                    "faceRect": [
                        {
                            "pointId": "left",
                            "point": d.left()
                        },
                        {
                            "pointId": "top",
                            "point": d.top()
                        },
                        {
                            "pointId": "right",
                            "point": d.right()
                        },
                        {
                            "pointId": "bottom",
                            "point": d.bottom()
                        }
                    ],
                    "shapeData": [],
                    "faceAngles": [
                        "{:7.2f}".format(euler_angle[0, 0]),
                        "{:7.2f}".format(euler_angle[1, 0]),
                        "{:7.2f}".format(euler_angle[2, 0])
                    ]
                }
                for (i, (x, y)) in enumerate(shape):
                    facePoints["shapeData"].append({
                        "pointId": i+1,
                        "pointX": x,
                        "pointY": y
                    })
                    cv2.circle(frame, (x, y), 2, (0, 0, 255), -1)
        
        fullMessage += "|@@##@@|{}".format(facePoints)
        newFrameBase64 = self.SendSocketMesg(fullMessage)
        newFrameBase64 = newFrameBase64.decode("utf-8")
        #print(newFrameBase64)
        #nparr = np.fromstring(base64.b64decode(newFrameBase64), np.uint8)
        #frame = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        #print(frame)
        return newFrameBase64
        #cv2.imshow("Face points", frame)
        #cv2.destroyAllWindows()
        #return frame
