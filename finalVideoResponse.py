import asyncio
import websockets
import cv2
import base64
import numpy as np
import socket

class VideoResponse:
    HOST = '127.0.0.1'  # The server's hostname or IP address
    PORT = 65433        # The port used by the server

    def SendSocketMesg(self, msg):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.HOST, self.PORT))
            s.sendall(str.encode(msg))
            data = s.recv(1024*128)
            return data
            #print("Response: {}".format(data))

    async def finalFrameResponse(self, websocket, path):
        image_base64 = await websocket.recv()

        print(image_base64)

        resp = self.SendSocketMesg(image_base64)

        await websocket.send(resp)

    def __init__(self):
        start_frame_server = websockets.serve(self.finalFrameResponse, "localhost", 9898)
        asyncio.get_event_loop().run_until_complete(start_frame_server)
        asyncio.get_event_loop().run_forever()


videoResp = VideoResponse()
