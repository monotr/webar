﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class VideoResponseSocket : WebArElement
{
    #region private members 	
    /// <summary> 	
    /// TCPListener to listen for incomming TCP connection 	
    /// requests. 	
    /// </summary> 	
    private TcpListener tcpListener;
    /// <summary> 
    /// Background thread for TcpServer workload. 	
    /// </summary> 	
    private Thread tcpListenerThread;
    /// <summary> 	
    /// Create handle to connected tcp client. 	
    /// </summary> 	
    private TcpClient connectedTcpClient;
    #endregion

    [Header("Texture")]
    public Renderer cameraPlane;
    Texture2D tex;
    public RenderTexture cameraRT;

    internal List<string> base64List;
    internal List<string> base64ResponsesList;
    int screenshotsToTake = 0;

    [Header("Face points")]
    public GameObject facePointPrefab;
    public Transform facePointsParent;
    List<RectTransform> facePointsRectTs;

    [Serializable]
    public class FaceRect
    {
        public string pointId;
        public int point;
    }
    [Serializable]
    public class Shape
    {
        public int pointId;
        public int pointX;
        public int pointY;
    }
    [Serializable]
    public class ShapesCollection
    {
        public int faceId;
        public List<FaceRect> faceRect;
        public List<Shape> shapeData;
        public List<string> faceAngles;
    }

    // Use this for initialization
    void Start()
    {
        base64List = new List<string>();
        base64ResponsesList = new List<string>();

        // Face points
        facePointsRectTs = new List<RectTransform>();
        GameObject newObj = null;
        for (int j = 0; j < 68; j++)
        {
            newObj = Instantiate(facePointPrefab, facePointsParent);
            newObj.name = "FacePoint_" + j;
            facePointsRectTs.Add(newObj.GetComponent<RectTransform>());
        }

        // Start TcpServer background thread 		
        tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();

        StartCoroutine(ApplyCameraTexture());
        StartCoroutine(TakeScreenshots());
        StartCoroutine(SendResponseFrame());
    }

    /// <summary> 	
    /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
    /// </summary> 	
    private void ListenForIncommingRequests()
    {
        try
        {
            // Create listener on localhost port 8052. 			
            tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 65433);
            tcpListener.Start();
            Debug.Log("Server is listening");
            Byte[] bytes = new Byte[1024 * 128];
            while (true)
            {
                using (connectedTcpClient = tcpListener.AcceptTcpClient())
                {
                    // Get a stream object for reading 					
                    using (NetworkStream stream = connectedTcpClient.GetStream())
                    {
                        int length;
                        // Read incomming stream into byte arrary. 						
                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            // Convert byte array to string message. 							
                            string clientMessage = Encoding.ASCII.GetString(incommingData);
                            Debug.Log("client message " + length + " received as: " + clientMessage);

                            screenshotsToTake++;
                        }
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("SocketException " + socketException.ToString());
        }
    }
    /// <summary> 	
    /// Send message to client using socket connection. 	
    /// </summary> 	
    private void SendSocketMessage(string serverMessage)
    {
        if (connectedTcpClient == null)
        {
            return;
        }

        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = connectedTcpClient.GetStream();
            if (stream.CanWrite)
            {
                // Convert string message to byte array.                 
                byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
                // Write byte array to socketConnection stream.               
                stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
                Debug.Log("Server sent his message - should be received by client");
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }

    IEnumerator ApplyCameraTexture()
    {
        while (true)
        {
            while (base64List.Count > 0)
            {
                string[] parts = base64List[0].Split('|');
                base64List.RemoveAt(0);
                print("Frames left : " + base64List.Count);

                byte[] b64_bytes = System.Convert.FromBase64String(parts[1]);

                Texture2D _tex = new Texture2D(1, 1);
                _tex.LoadImage(b64_bytes);

                cameraPlane.material.mainTexture = _tex;

                // Face points
                if(parts[3].Length > 2)
                {
                    float x = 0;
                    float y = 0;
                    Vector2 cameraDimensions = new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight);
                    parts[3] = parts[3].Replace("'", "\"");
                    ShapesCollection sc = JsonUtility.FromJson<ShapesCollection>(parts[3]);
                    for (int i = 0; i < facePointsRectTs.Count; i++)
                    {
                        x = (sc.shapeData[i].pointX / cameraDimensions[0]) * Camera.main.pixelWidth;
                        y = Camera.main.pixelHeight - ((sc.shapeData[i].pointY / cameraDimensions[1]) * Camera.main.pixelHeight);
                        facePointsRectTs[i].position = new Vector3(x, y, facePointsRectTs[i].position.z);
                    }
                }

                //Destroy(_tex);
            }

            yield return null;
        }
    }

    IEnumerator TakeScreenshots()
    {
        while (true)
        {
            if (screenshotsToTake > 0)
            {
                screenshotsToTake--;

                // Take new photo
                int width = cameraRT.width;
                int height = cameraRT.height;

                string newBase64 = string.Empty;
                try
                {
                    RenderTexture rt = new RenderTexture(width, height, 24);
                    Camera.main.targetTexture = rt;
                    Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
                    Camera.main.Render();
                    RenderTexture.active = rt;
                    screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                    Camera.main.targetTexture = null;
                    RenderTexture.active = null;
                    Destroy(rt);
                    byte[] bytes = screenShot.EncodeToPNG();
                    newBase64 = System.Convert.ToBase64String(bytes);

                    /*Camera.main.targetTexture = cameraRT;
                    RenderTexture.active = cameraRT;
                    Camera.main.Render();
                    ScreenCapture.CaptureScreenshot("SomeLevel.png");
                    byte[] fileData = File.ReadAllBytes("SomeLevel.png");
                    newBase64 = System.Convert.ToBase64String(fileData);*/
                }
                catch (Exception e)
                {
                    print(e.Message);
                }

                base64ResponsesList.Add(newBase64);

                /*Camera.main.targetTexture = cameraRT;
                Camera.main.Render();
                _tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
                _tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                _tex.Apply();
                string newBase64 = System.Convert.ToBase64String(_tex.EncodeToPNG());*/
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator SendResponseFrame()
    {
        while (true)
        {
            if (base64ResponsesList.Count > 0)
            {
                SendSocketMessage(base64ResponsesList[0]);
                base64ResponsesList.RemoveAt(0);
            }
            yield return null;
        }
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
}
