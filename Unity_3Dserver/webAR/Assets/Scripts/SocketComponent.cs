﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class SocketComponent : MonoBehaviour
{
    #region private members 	
    /// <summary> 	
    /// TCPListener to listen for incomming TCP connection 	
    /// requests. 	
    /// </summary> 	
    private TcpListener tcpListener;
    /// <summary> 
    /// Background thread for TcpServer workload. 	
    /// </summary> 	
    private Thread tcpListenerThread;
    /// <summary> 	
    /// Create handle to connected tcp client. 	
    /// </summary> 	
    private TcpClient connectedTcpClient;
    #endregion

    [Header("Texture")]
    public Renderer cameraPlane;
    Texture2D tex;
    public RenderTexture cameraRT;

    public GameObject[] rotatingObjects;

    List<string> base64List;

    // Use this for initialization
    void Start()
    {
        base64List = new List<string>();

        // Start TcpServer background thread 		
        tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();

        StartCoroutine(ApplyCameraTexture());
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var item in rotatingObjects)
        {
            item.transform.Rotate(Vector3.one * UnityEngine.Random.Range(0, 100) * Time.deltaTime);
        }
    }

    /// <summary> 	
    /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
    /// </summary> 	
    private void ListenForIncommingRequests()
    {
        try
        {
            // Create listener on localhost port 8052. 			
            tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 65432);
            tcpListener.Start();
            Debug.Log("Server is listening");
            Byte[] bytes = new Byte[1024*128];
            while (true)
            {
                using (connectedTcpClient = tcpListener.AcceptTcpClient())
                {
                    string fullMsg = string.Empty;
                    print("=== New message ===");
                    // Get a stream object for reading 					
                    using (NetworkStream stream = connectedTcpClient.GetStream())
                    {
                        int length;
                        // Read incomming stream into byte arrary. 						
                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            // Convert byte array to string message. 							
                            string clientMessage = Encoding.ASCII.GetString(incommingData);
                            //Debug.Log("client message " + length + " received as: " + clientMessage);

                            fullMsg += clientMessage;

                            if (clientMessage.Contains("@@!!@@"))
                            {
                                fullMsg = fullMsg.Replace("@@!!@@", "");
                                //print("FULL MSG: " + fullMsg);

                                string[] parts = fullMsg.Split(',');

                                base64List.Add(parts[1]);
                            }
                        }
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("SocketException " + socketException.ToString());
        }
    }
    /// <summary> 	
    /// Send message to client using socket connection. 	
    /// </summary> 	
    private void SendSocketMessage(string serverMessage)
    {
        if (connectedTcpClient == null)
        {
            return;
        }

        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = connectedTcpClient.GetStream();
            if (stream.CanWrite)
            {
                // Convert string message to byte array.                 
                byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
                // Write byte array to socketConnection stream.               
                stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
                Debug.Log("Server sent his message - should be received by client");
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }

    IEnumerator ApplyCameraTexture()
    {
        while (true)
        {
            if(base64List.Count > 0)
            {
                byte[] b64_bytes = System.Convert.FromBase64String(base64List[0]);
                base64List.RemoveAt(0);
                print("Frames left : " + base64List.Count);

                Texture2D _tex = new Texture2D(1, 1);
                _tex.LoadImage(b64_bytes);

                cameraPlane.material.mainTexture = _tex;

                // Take new photo
                int width = cameraRT.width;
                int height = cameraRT.height;

                yield return new WaitForEndOfFrame();

                /*RenderTexture rt = new RenderTexture(width, height, 24);
                Camera.main.targetTexture = rt;
                Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
                Camera.main.Render();
                RenderTexture.active = rt;
                screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                Camera.main.targetTexture = null;
                RenderTexture.active = null; // JC: added to avoid errors
                Destroy(rt);
                byte[] bytes = screenShot.EncodeToPNG();
                string newBase64 = System.Convert.ToBase64String(bytes);*/

                /*Camera.main.targetTexture = cameraRT;
                Camera.main.Render();
                RenderTexture.active = cameraRT;
                ScreenCapture.CaptureScreenshot("SomeLevel.png");
                byte[] fileData = File.ReadAllBytes("SomeLevel.png");
                string newBase64 = System.Convert.ToBase64String(fileData);*/

                Camera.main.targetTexture = cameraRT;
                Camera.main.Render();
                _tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
                _tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                _tex.Apply();
                string newBase64 = System.Convert.ToBase64String(_tex.EncodeToPNG());

                SendSocketMessage(newBase64);

                Destroy(_tex);
            }

            yield return null;
        }
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
}
