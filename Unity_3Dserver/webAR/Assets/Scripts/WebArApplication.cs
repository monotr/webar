﻿using UnityEngine;

public class WebArElement : MonoBehaviour
{
    // Gives access to the application and all instances.
    internal WebArApplication app;

    private void Awake()
    {
        app = FindObjectOfType<WebArApplication>();
    }
}

public class WebArApplication : MonoBehaviour
{
    public VideoFeedSocket videoFeedSocket;
    public VideoResponseSocket videoResponseSocket;
}
