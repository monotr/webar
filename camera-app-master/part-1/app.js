// Set constraints for the video stream
var constraints = { video: { facingMode: "user" }, audio: false };
var track = null;

// Define constants
const cameraView = document.querySelector("#camera--view"),
    cameraOutput = document.querySelector("#camera--output"),
    cameraSensor = document.querySelector("#camera--sensor"),
    cameraTrigger = document.querySelector("#camera--trigger");

// Access the device camera and stream to cameraView
function cameraStart() {
    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(function(stream) {
            track = stream.getTracks()[0];
            cameraView.srcObject = stream;

            takeSnap();
            receiveFrame();
        })
        .catch(function(error) {
            console.error("Oops. Something is broken.", error);
        });
}

// Take a picture when cameraTrigger is tapped
function takeSnap() {
    cameraSensor.width = cameraView.videoWidth;
    cameraSensor.height = cameraView.videoHeight;
    cameraSensor.getContext("2d").drawImage(cameraView, 0, 0);
    
    //console.log("taken: ", cameraSensor.toDataURL("image/webp"))

    const connection = new WebSocket('ws://127.0.0.1:9998');
    connection.onopen = () => {
        //console.log('connected');
        connection.send(cameraSensor.toDataURL("image/webp"));
    };
    connection.onclose = (error) => {
        //console.error('disconnected', error);
        takeSnap();
    };
    connection.onerror = (error) => {
        console.error('failed to connect', error);
    };
    connection.onmessage = (event) => {
        //console.log('received', event.data);
    };
    
    // track.stop();
};

// 
function receiveFrame() {

    const connection = new WebSocket('ws://127.0.0.1:9898');
    
    connection.onopen = () => {
        console.log('connected');
        connection.send("Gimme frame!");
    };
    connection.onclose = (error) => {
        console.error('disconnected', error);
        receiveFrame();
    };
    connection.onerror = (error) => {
        console.error('failed to connect', error);
    };
    connection.onmessage = (event) => {
        console.log('received', event.data);
        console.log("blob url: ", URL.createObjectURL(event.data));
        cameraOutput.src = URL.createObjectURL(event.data);
        cameraOutput.classList.add("taken");
    };

    // track.stop();
};

// Start the video stream when the window loads
window.addEventListener("load", cameraStart, false);