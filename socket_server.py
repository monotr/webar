import socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('localhost', 9998))
s.listen(1)
print("list - {}".format(s.getsockname()))
conn, addr = s.accept()
print("Socket ready - {} - {}".format(conn, addr))
while 1:
    data = conn.recv(1024)
    if not data:
        continue
    print(data)
    conn.sendall(data)
conn.close()
