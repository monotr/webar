import asyncio
import websockets
import cv2
import base64
import numpy as np
from facePoints import Points

class VideoFeed:

    ENCODE_PARAM = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

    def data_uri_to_cv2_img(self, base64_string):
        base64_string = base64_string.split(',')[1]
        nparr = np.fromstring(base64.b64decode(base64_string), np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        """cv2.imshow("recv", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()"""
        return img

    async def receiveCameraFeed(self, websocket, path):
        image_base64 = await websocket.recv()

        if "base64" in image_base64:
            #print(image_base64)
            img = self.data_uri_to_cv2_img(image_base64)
            
            cv2.putText(img, "popo", (140, 250), cv2.FONT_HERSHEY_SIMPLEX, .5,(255, 255, 255), 2, cv2.LINE_AA)

            """cv2.imshow("text", img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()"""

            fullImg = self.points.GetFacePoints(img)

            #result, buffer = cv2.imencode('.jpg', img, ENCODE_PARAM)
            #jpg_as_text = base64.b64encode(buffer)

            #resp = image_base64.split(',')[0] + "," + jpg_as_text.decode("utf-8")
            #resp = image_base64.split(',')[0] + "," + fullImg
            #print(" === RESPONSE : {}".format(resp))
            resp = ""
            await websocket.send(resp)

    def __init__(self):
        self.points = Points()

        start_server = websockets.serve(self.receiveCameraFeed, "localhost", 9998)
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()


videoFeed = VideoFeed()